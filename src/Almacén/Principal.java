
package Almacén;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class Principal {

    private static Scanner e = new Scanner(System.in);
    private static List<Producto> productos;
    private static Producto pro;
    //private static Venta venta;

    
     //NOTA: LOS DESCUENTOS DEBEN REGISTRARSE EN DECIMALES Y PARA ELLO EL SEPARADOR DECIMAL DEBE SER COMA(,)
     //Ejemplo: 0,25
    
    public static void main(String[] args) {
        int ind = -1;
        productos = new ArrayList<Producto>();
        //venta = new Venta();
        while (ind != 0) {
            System.out.print("\t Menu  \n"
                    + "1. Registrar producto \n"
                    + "2. Registrar stocks de productos \n"
                    + "3. Listar productos \n"
                    + "4. Vender productos \n"
                    + "5. Buscar ventas"
                    + "0. Salir \n\n"
                    + "Ingrese una opcion: ");
            ind = e.nextInt();
            switch (ind) {
                case 1: registrarProductos(); break;
                case 2: registrarStockProductos(); break;
                case 3: listarProductos(); break;
                case 4: venderProductos(); break;
                case 5: buscarVentas(); break;
                case 0: despedida(); break;
                default: System.out.println("Opcion invalida!"); break;
            }
        }
    }

    public static void registrarProductos() {
        Producto producto = new Producto();
        System.out.print("Ingrese descripcion: ");
        producto.setDescripcion(e.next());
        System.out.print("Ingrese stock: ");
        producto.setStock(e.nextInt());
        do{
            System.out.print("Ingrese tipo de producto: ");
            producto.setTipo(e.next());
            producto.selTipo(producto.getTipo().trim());
        }while(producto.getPrecio() == 0);
        productos.add(producto);
    }

    public static void registrarStockProductos() {
        pro = pro == null ? new Producto() : pro;
        System.out.print("Ingrese el codigo del producto:  ");
        int id = e.nextInt();
        pro = pro.buscar(productos, id);
        if (pro != null) {
            System.out.print("Ingrese la cantidad de stock que desea agregar:  ");
            int stock = e.nextInt();
            pro.agregarStock(pro, stock);
        } else {
            System.out.println("");
            System.out.println("No existe el producto seleccionado ");
            System.out.println("");
        }
    }

    public static void listarProductos() {
        if (!productos.isEmpty()) {
            for (Producto lst : productos) {
                System.out.println(lst);
            }
        } else {
            System.out.println("");
            System.out.println("No existen productos registrados ");
            System.out.println("");
        }
    }

    public static void venderProductos() {
        pro = pro == null ? new Producto() : pro;
        String rpt;
        int cant;
        List<Producto> productosAgregadosVenta = new ArrayList<Producto>();
        List<Venta> lstVentas = new ArrayList<Venta>();
        do {
            System.out.print("Ingrese el codigo del producto a vender: ");
            int idP = e.nextInt();
            pro = pro.buscarProducto(productos, idP);
            if (pro != null) {
                System.out.print("Ingrese la cantidad a comprar: ");
                cant = e.nextInt();
                if (pro.getStock() > cant) {
                    pro.actualizarStock(cant);
                                        
                    productosAgregadosVenta.add(new Producto (pro.getId(),pro.getDescripcion(),pro.getPrecio(), pro.getDescuento(), cant));
                    //venta.agregar(pro, cant);
                                        
                    System.out.print("DESEA AGREGAR OTRO PRODUCTO S/N: ");
                    rpt = e.next();
                } else {
                    System.out.println("");
                    System.out.println("No existe suficiente stock para abastecer el pedido ");
                    System.out.println("");
                    break;
                }
            } else {
                System.out.println("");
                System.out.println("No existe el producto seleccionado ");
                System.out.println("");
                break;
            }
        } while (rpt.equalsIgnoreCase("S"));
        if (pro != null) {
            System.out.println("");
            Venta venta = new Venta(productosAgregadosVenta);            
            venta.resumenVenta(productos, productosAgregadosVenta);
            lstVentas.add(venta);
        }
    }

    public static void buscarVentas(){
        
        
        
    }
    
    
    public static void despedida() {
        System.out.println("");
        System.out.println("Fin de la aplicacion ");
        System.out.print("Autores: \n"
                + "\tNombre1 \n"
                + "\tNombre2 \n"
                + "\tNombre3 \n"
                + "\tNombre4 \n");
    }
}
