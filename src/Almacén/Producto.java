
package Almacén;

import java.util.List;


public class Producto {
    
    
    private int id;
    private static int idSiguiente;
    private String tipo;
    private String descripcion;
    private double precio;
    private double descuento;
    private int stock;

    public Producto(){ this.id = ++idSiguiente;  }
    
    
    
    public Producto(String tipo, String descrip, double precio, double desc, int stock){
        this();//this.id = ++idSiguiente;
        this.tipo = tipo;
        this.descripcion = descrip;
        this.precio = precio;
        this.descuento = desc;
        this.stock=stock;
    }
   
    public Producto(int id, String descrip, double precio, double desc, int stock){
        this.id = id;        
        this.descripcion = descrip;
        this.precio = precio;
        this.descuento = desc;
        this.stock=stock;
    }
    
    @Override
    public String toString(){
        return "Producto { id = " + id + ", tipo= " + tipo +", descripcion= " + descripcion + ", precio= " + precio + ", descuento= " + descuento + ", stock= " + stock + " }";
    }
    
    public Producto buscar(List<Producto> productos, int id){
           for(Producto lst: productos){
               if(lst.getId() == id){
                   System.out.println(lst);
                   return lst;
               }
           }
           return null;
    }
    
    public Producto buscarProducto(List<Producto> productos, int id){
           for(Producto lst: productos){
               if(lst.getId() == id){
                   System.out.println("Nombre: " + lst.getDescripcion() + " Precio: " + lst.getPrecio());
                   return lst;
               }
           }
           return null;
    }
    
    public void agregarStock(Producto producto, int stock){
        this.stock = this.stock + stock;
        System.out.println(producto);                                  
    }    
    
    public void actualizarStock(int cantidadIngresada){
        this.stock = this.stock - cantidadIngresada;
    }
    
    public void selTipo(String tipo){
       double p = 0;
       double d = 0;
       
       if(tipo.equalsIgnoreCase("A")){ p = 27.49; d = 0.25; }
        
       if(tipo.equalsIgnoreCase("B")){ p = 16.43; d = 0.20; }
       
       if(tipo.equalsIgnoreCase("C")){ p = 12.28; d = 0.15; }
       
       this.precio = p;
       this.descuento = d;
       
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public double getDescuento() {
        return descuento;
    }

    public void setDescuento(double descuento) {
        this.descuento = descuento;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
    

    
}
