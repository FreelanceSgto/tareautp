
package Almacén;

import java.util.ArrayList;
import java.util.List;


public class Venta {
    private int id;
    private List<Producto> productos;
    private static int idSiguiente;

    public Venta(List<Producto> productos){
        this.id = ++idSiguiente;
        this.productos = productos;
    }
    
    
    /**
    public Venta(){
        productos = new ArrayList<Producto>();
    }
        
    public void agregar(Producto producto, int cantidadVendida ){
        productos.add(new Producto(producto.getId(),producto.getDescripcion(),producto.getPrecio(), producto.getDescuento(), cantidadVendida));
    }
   **/
    
    public void buscarVenta(){
        
        
        
    }
    
    public void resumenVenta(List<Producto> productosRegistrados, List<Producto> productos){
        int id;
        double subtotal = 0, total = 0;
        int cantidad = 0;
        System.out.println("Articulo\t Precio\t  Cantidad\t Descuento\t SubTotal");
        for(Producto lst: productosRegistrados){
            id = lst.getId();
            for(Producto lst2: productos){
                if(id ==lst2.getId()){                    
                    subtotal  = subtotal + ((lst2.getPrecio() - (lst2.getPrecio() * lst2.getDescuento())) * lst2.getStock());
                    cantidad = cantidad + lst2.getStock();
                }
            }
            if(subtotal > 0){
                System.out.println( lst.getDescripcion() + "\t\t " + lst.getPrecio() + "\t\t " + cantidad + "\t\t " + lst.getDescuento() + "\t\t " + subtotal);
                total = total + subtotal;
            }
            subtotal = 0;
            cantidad = 0;
        }
        System.out.println("");
        System.out.println("EL TOTAL DE LA VENTA ES: " + total);
        productos.clear();
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }
    
    
}
